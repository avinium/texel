import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule }   from '@angular/router';
import { HttpModule, Http } from '@angular/http';
import { AppComponent } from './app.component';
import { File, FileList } from './file'
import { Label, LabelList,labelListFactory  } from './label'
import { ConfirmViewComponent } from './confirm-view.component';
import { FileDisplayComponent } from './file-display.component';
import { FileListViewComponent } from './file-list-view.component';
import { FileComparisonViewComponent } from './file-comparison-view.component';
import { FileListElementViewComponent } from './file-list-element-view.component';
import { LabelListViewComponent } from './label-list-view.component';
import { LabelListElementViewComponent } from './label-list-element-view.component';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

@NgModule({
  declarations: [
    AppComponent,
	ConfirmViewComponent,
    FileComparisonViewComponent,
	FileDisplayComponent,
	FileListViewComponent,
	FileListElementViewComponent,
	LabelListViewComponent,
	LabelListElementViewComponent
  ],
  imports: [
    BrowserModule,
	HttpModule,
	RouterModule.forRoot([
      {
        path: 'files',
        component: AppComponent
      }
    ]),
	TabsModule.forRoot(),
	NgbModule.forRoot(),
	BsDropdownModule.forRoot()
  ],
  providers: [
	{ 
		provide: LabelList,
		deps: [Http],
		useFactory: labelListFactory
	}],
  bootstrap: [AppComponent],
})
export class AppModule { }
