import { OnInit, OnChanges, OnDestroy, Component, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { AppComponent } from './app.component';
import { File, FileList } from './file';
import { Label, LabelList  } from './label';

@Component({
	selector: 'file-comparison-view',
	templateUrl: './file-comparison-view.component.html',
	styleUrls: ['./app.component.css','./file-comparison-view.component.css']
})

export class FileComparisonViewComponent {
	@Input() files: File[];
    @Input() closeEmitter: EventEmitter<boolean>;
    @Input() label: Label;
    selectedFile: File;
	constructor() {}


}
