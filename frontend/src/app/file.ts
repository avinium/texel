import { OnInit, OnDestroy, Component, ViewChild, EventEmitter, Input, Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Label, LabelList  } from './label';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class File {
	
	id: string;
	filepath: string;
	filename: string;
    complete: boolean;
    corrupt: boolean;

    private _labels: Promise<Label[]>;

    get labels(): Promise<Label[]> {
		if(!this._labels) {
			this._labels = this.http.get('/files/'+encodeURIComponent(this.id)+'/labels')
							.toPromise()
							.then(response => {
								 return (response.json() as Label[]).map(fileLabel => this.labelList.find(fileLabel.id))
							});
		}
        return this._labels;
    }
	
	set labels(labels: Promise<Label[]>) {
		this._labels = labels;
	}
	
	constructor(private http:Http, private labelList: LabelList, srcObject) {
		this.http = http;
		this.labelList = labelList;
		this.id = srcObject.id;
        this.complete = srcObject.complete;
        this.corrupt = srcObject.corrupt;
		this.filepath = srcObject.filepath;
		this.filename = srcObject.filename;	
	}	
	containsAny(labels: Label[]): Promise<boolean> {
        return this.labels.then((thisLabels) => { 
            return labels.some(label => thisLabels.includes(label));
        });
	}
	contains(label: Label): Promise<boolean> {
		return this.containsAny([label]);
	}
	getLabelsRegex(): Promise<any> {
		return this.labels.then((labels) => {
			return LabelList.toRegex(labels);
		});
	}
	toggleLabel(label: Label): Promise<any> {
		return this.contains(label).then((contains) => {
            return (contains) ? this.removeLabel(label) : this.addLabel(label)
        });
	}
    markComplete() {
        return this.http.post('/files/'+encodeURIComponent(this.id)+'/complete', {}, new Headers({'Content-Type': 'application/x-www-urlencoded'}))
                .toPromise()
                .then(response => {
                    this.complete = true;
                });
    }
    markIncomplete() {
        return this.http.post('/files/'+encodeURIComponent(this.id)+'/incomplete', {}, new Headers({'Content-Type': 'application/x-www-urlencoded'}))
                .toPromise()
                .then(response => {
                    this.complete = false;
                });
    }
    markCorrupt() {
        return this.http.post('/files/'+encodeURIComponent(this.id)+'/corrupt', {}, new Headers({'Content-Type': 'application/x-www-urlencoded'}))
                .toPromise()
                .then(response => {
                    this.corrupt = true;
                });
    }
    markIncorrupt() {
        return this.http.post('/files/'+encodeURIComponent(this.id)+'/incorrupt', {}, new Headers({'Content-Type': 'application/x-www-urlencoded'}))
                .toPromise()
                .then(response => {
                    this.corrupt = false;
                });
    }
    toggleComplete() {
        return (this.complete) ? this.markIncomplete() : this.markComplete();
    }
    toggleCorrupt() {
        return (this.corrupt) ? this.markIncorrupt() : this.markCorrupt();
    }
	removeLabel(label: Label): Promise<void> {                              
		return this.http.post('/files/'+encodeURIComponent(this.id)+'/labels/remove', {label_name:label.name}, new Headers({'Content-Type': 'application/x-www-urlencoded'}))
                .toPromise()
                .then(response => {
                    this.labels.then((labels) => {
                        this.labels = new Promise((resolve, reject) => {
                            resolve(labels.filter(obj => obj != label));
                        });
                    });
                });
	}
    
	addLabel(label: Label): Promise<void> {
		return this.http.post('/files/'+encodeURIComponent(this.id)+'/labels/add', {label_name:label.name}, new Headers({'Content-Type': 'application/x-www-urlencoded'}))
                            .toPromise()
                            .then(response => { 
                                this.labels.then((labels) => { 
                                    labels.push(this.labelList.find(label.id));
                                    this.labels = new Promise((resolve, reject) => {
                                        resolve(labels);
                                    });
                                });
                            });
        
	}
}

@Injectable()
export class FileList {
	
	count: Promise<number>;
	files: Promise<File[]>;
	
	constructor(private http:Http, private labelList: LabelList, labelled) { 	
		let url = (labelled) ? "files/labelled" : "files/unlabelled";
		this.files = this.http.get(url).toPromise().then((response) => { 
			this.count = new Promise((resolve, reject) => { resolve(response.json().length); });
			return response.json().map((obj) => { return new File(http, labelList, obj) });
		});

	}
	
	containsAny(files: File[]): Promise<boolean> {
        return this.files.then((thisFiles) => { 
            return files.some(file => thisFiles.includes(file));
        });
	}
	contains(file: File): Promise<boolean> {
		return this.containsAny([file]);
	}
    
    merge(newFiles:File[]): void {        
        this.files.then((filez) => {
            this.files = new Promise((resolve, reject) => {
                this.count.then((oldCount) => {
                   this.count = new Promise((resolve, reject) => { resolve(oldCount + newFiles.length); });
                }); 
                resolve(filez.concat(newFiles));    
            });
        });
    }
    
    remove(file:File): Promise<File> {
        return this.files.then((filez) => {
            this.files = new Promise((resolve, reject) => {
                filez.splice(filez.indexOf(file), 1)
                this.count.then((oldCount) => {
                   this.count = new Promise((resolve, reject) => { 
						if(oldCount == 0) {
							resolve(oldCount); 	
						} else {
							resolve(oldCount-1); 	
						}
					});
                });                        
                resolve(filez);    
                
            });
            return file;
        });
    }
			
	resetLabels(): Promise<File[]> {
		return this.http.post('/files/labels/remove', {}, new Headers({'Content-Type': 'application/x-www-urlencoded'}))
				.toPromise()
				.then(response => { 
                    return this.files.then((filez) => {
                        this.count.then((oldCount) => {
                           this.count = new Promise((resolve, reject) => { resolve(0); });
                        });                        
                        this.files = new Promise((resolve,reject) => { return []});
                        return filez;    
                    });
                });
	}
			
}


