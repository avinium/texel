import { OnInit, OnDestroy, Component, ViewChild, EventEmitter, Input, Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { File, FileList } from './file';
import { Label, LabelList  } from './label';
import 'rxjs/Rx';
import { TabsetComponent } from 'ngx-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@Component({
	selector: 'app-root',
	host: { '(document:click)': 'onClick($event)' },
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css'],
})

export class AppComponent implements OnInit {
	
	activeLabels: Label[];
    documentClickEmitter = new EventEmitter<Object>();
    closeComparisonViewEmitter = new EventEmitter<boolean>();
    confirmEmitter = new EventEmitter<Object>();
    compareLabel: Label;
    displayFile: File;
	displayFileIsLabelled: Promise<boolean>;
    compareFiles: Promise<File[]>;
	unlabelledFileList: FileList;
	labelledFileList: FileList;
    viewAllEmitter = new EventEmitter<Label>();
	fileContentsRightClickEmitter = new EventEmitter<Object>();
    
    selectedFiles = new Array() as File[];        
    
	constructor(private http:Http, private labelList:LabelList) {    }
	
	ngOnInit(): void {
        this.activeLabels = [];
		this.unlabelledFileList = new FileList(this.http, this.labelList, false);
		this.labelledFileList = new FileList(this.http, this.labelList, true);
        this.viewAllEmitter.subscribe((label: Label) => {
            this.compareFiles = (label as Label).getFiles(this.labelledFileList);
            this.compareLabel = label;
        });
        this.closeComparisonViewEmitter.subscribe((close) => {
            if(close) {
                this.compareFiles = null;
            }
        });
		this.displayFileIsLabelled = new Promise((resolve, reject) => {return false });
		
		this.fileContentsRightClickEmitter.subscribe((object) => {
			if(this.labelList.findByName(object.selectedText) == null) {
				this.labelList.save(object.selectedText).then((label) => {
					this.displayFile.addLabel(label).then(() =>{
						object.onSuccess.emit(label);	
					});
				});
			}
		});
	}
	
	onClick(event) {
        this.documentClickEmitter.emit(event)
	}
	
	nextFile(): void {
        this.unlabelledFileList.remove(this.selectedFiles[0]).then((file) => {
            this.labelledFileList.merge([file]);
            this.unlabelledFileList.files.then((filez) => {
                this.displayFile = filez[0];    
                this.selectedFiles = [filez[0]];
            });
        })
	}
	
    handleError(err: any): void {
		var dismiss = new EventEmitter<boolean>();
		this.confirmEmitter.emit({requestString:err, responseEmitter: dismiss});
    }
	
	select(file: File) {
        this.displayFile = file;
		this.selectedFiles = [file]; 
		this.displayFileIsLabelled = this.labelledFileList.contains(file);
	}

    toggleLabel(label: Label, file:File) {
         file.toggleLabel(label).then(() => {
            
         }).catch((e) => { this.handleError(e) }); 
         
    }
}


