import { OnInit, OnDestroy, Component, ViewChild, EventEmitter, Input } from '@angular/core';
import { AppComponent } from './app.component';
import { File, FileList } from './file';
import { Label, LabelList  } from './label';

@Component({
	selector: 'file-list-element-view',
	templateUrl: './file-list-element-view.component.html',
	styleUrls: ['./app.component.css'],
})

export class FileListElementViewComponent {
    
	@Input() label: Label;
	@Input() labelList: LabelList;
	@Input() file: File;
    @Input() selectedFiles: File[];
	@Input() errorEmitter: EventEmitter<any>;
	@Input() selectEmitter: EventEmitter<any>;
	openDropdown: boolean;
	
	constructor() { }
	
	toggleDropdown(e, label): void {
		e.preventDefault();
		e.stopPropagation();
		this.openDropdown = !this.openDropdown;
	}
    
    ngOnInit() {       
    }
	
	
}