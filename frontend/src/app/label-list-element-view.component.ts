import { OnInit, OnDestroy, OnChanges, Component, ViewChild, EventEmitter, Input, Output, ElementRef } from '@angular/core';
import { AppComponent } from './app.component';
import { File, FileList } from './file';
import { Label, LabelList  } from './label';


@Component({
	selector: 'label-list-element-view',
	templateUrl: './label-list-element-view.component.html',
	styleUrls: ['./app.component.css'],
})

export class LabelListElementViewComponent {
	
	@Input() documentClickEmitter;
    @Input() confirmEmitter: EventEmitter<Object>;
    @Input() file: File;
    @Input() isActive: boolean;
	@Input() label: Label;
	@Input() labelList: LabelList;
	@Input() selectEmitter: EventEmitter<Object>;
    @Input() viewAllEmitter: EventEmitter<Label>;
    
    openDropdown = false;
	
	
	constructor(myElement: ElementRef) { 
		
	}
    
    // clicks anywhere else on the document should close the dropdown menu if it' open
    ngOnInit() {       
        this.documentClickEmitter.subscribe((clickEvent) => {
            this.openDropdown = false;
        });
    }
    
    // but we don't want clicks on the dropdown menu itself to close it, so we will stop any click events propagating up
    overrideClick(event) {
        event.preventDefault();
		event.stopPropagation();
    }
    		
	toggleDropdown(e, label): void {
		e.preventDefault();
		e.stopPropagation();
		this.openDropdown = !this.openDropdown;
	}
	
	viewAll(event): void {
        this.overrideClick(event); 
		this.viewAllEmitter.emit(this.label);
	}
	del(event): void {		
        var thisComponent = this;
        this.overrideClick(event); 
		var onConfirm = new EventEmitter<boolean>();
		onConfirm.subscribe((response) => {
			if(response.response == true) {
				this.labelList.del(this.label).catch((e) => { 
                    console.error(e);
                    thisComponent.confirmEmitter.emit({requestString:e}); 
                });
			}
		});
		this.confirmEmitter.emit({responseEmitter:onConfirm, requestString:"Please confirm OK to delete."});
	}
	
	rename(): void  {
		var onConfirm = new EventEmitter<Object>();
		onConfirm.subscribe((response) => {
            console.log(response)
			if(response.response == true) {
				this.label.rename(response.responseInput).catch((e) => { this.confirmEmitter.emit({requestString:e}) });
			}
		});
		this.confirmEmitter.emit({responseEmitter:onConfirm, requestString:"Enter new label name:", requestInput:true})
	}
    select(event): void {
        this.overrideClick(event); 
        this.selectEmitter.emit(this.label);
    }
	
}