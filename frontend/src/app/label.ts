import { OnInit, OnDestroy, Component, ViewChild, EventEmitter, Input, Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { File, FileList } from './file';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';


export class Label {
	id: string;
	name: string;
	
	constructor(private http:Http, props:Object) { 
        this.id = props['id'];
        this.name = props['name'];
	}

	getFiles(fileList: FileList): Promise<File[]> {
        var matched = []
        return fileList.files.then((filez) => { 
            for(let file of filez) {
                file.labels.then((labelz) => {
                    if(labelz.some(label => label==this)) {
                        matched.push(file);
                    }
                });
            }
            return matched;
        });
	}
	del(): Promise<Response> {
	  return this.http.post('/labels/'+this.id+'/delete', {label_name:this.name}, new Headers({'Content-Type': 'application/x-www-urlencoded'})).toPromise();
	}
	rename(newName: string): Promise<void> {
		return this.http.post('/labels/'+this.id+'/rename', {newName:newName}, new Headers({'Content-Type': 'application/x-www-urlencoded'}))
			.toPromise()
			.then(response => {
				this.name = newName;
			})
	}
}

@Injectable()
export class LabelList {
	
	labels: Label[];
	
	constructor(private http:Http) { 
		this.labels = [];
		this.fetch().then((response) => {
			this.labels = response.json().map((obj) => { return new Label(http, obj) });
			this.sort();
		});		
	}
		
	del(label: Label) {
        var thisComponent = this;
		return label.del().then((response) => {
			thisComponent.labels = thisComponent.labels.filter((obj) => obj != label);
		});
	}
	delAll(): Promise<Response> {
		return this.http.post('/labels/deleteAll', {name}, new Headers({'Content-Type': 'application/x-www-urlencoded'}))
                    .toPromise()
                    .then((response) => {
                       this.labels = [];
                       return response;
                    });
	}
	fetch(): Promise<Response> {
		return this.http.get('/labels').toPromise()
	}
	find(id: string): Label {
		return this.labels.find((label) => label.id == id)
	}
	findByName(name: string): Label {
		return this.labels.find((label) => label.name == name)
	}
	
	save(name: string): Promise<Label> {
		let saved = this.findByName(name);
		if(saved != null) {
			return new Promise((resolve, reject) => {
				resolve(saved);
			});
		} else {
			return this.http.post('/labels', {name:name}, new Headers({'Content-Type': 'application/x-www-urlencoded'}))
						.toPromise()
						.then(response => { 
							saved = new Label(this.http, response.json()[0]);
							this.labels.push(saved); 
							this.sort();
							return saved;
						});
		}
	}
	sort() {
		this.labels.sort((a,b) => {
			if(a.name < b.name) return -1;
			if(a.name > b.name) return 1;
			return 0;
		});
	}
	static toRegex(labels: Label[]) {
		if(labels.length == 0) {
			return null;
		}
		let regexString = "";
		for (let i = 0; i < labels.length; i++) {
			regexString += labels[i].name.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
			if(i < labels.length - 1) {
				regexString += "|";
			}
		}
		return new RegExp(regexString,"g");
		
	}
}

export function labelListFactory(http:Http) {
         return new LabelList(http) 	
}
