import { FileDisplayComponent } from './file-display.component';
import { Headers, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import { OnInit, OnChanges, OnDestroy, Component, ViewChild, EventEmitter, Input, ElementRef } from '@angular/core';
    
fdescribe('File Display Unit Tests', () => {

        var tag1 = "<div style=\"left: 238.104px; top: 209.455px; font-size: 39.6px; font-family: sans-serif; transform: scaleX(0.880714);\">"
//        tag1 = "<div>"
        var tag2 = "</div>";
        var text1 = "Some ";
        var text2 = "notice ";
        var text3 = "text";
        var fileDisplay;
        var openTag = "<span class='highlight'>"    
        var closeTag = "</span>";
        var innerHTML = tag1 + text1 + tag2 + tag1 + text2 + tag2 + tag1 + text3 + tag2;
        var innerText = text1 + text2 + text3;

//        tag1 = "<1>"
//        tag2 = "</1>"
//        text1 = "a"
//        text2 = "b"
//        text3 = "c"
//        openTag = "<o>"
//        closeTag = "</o>"

        fileDisplay = new FileDisplayComponent({} as DomSanitizer, {} as Http, {} as ElementRef);

//    it('extract html indices', () => {
//        let [htmlIndices, htmlTags] = fileDisplay.splitHtml(innerHTML);
//        let offset1 = tag1.length+text1.length;
//        let offset2 = offset1 + tag2.length;
//        let offset3 = offset2 + tag1.length + text2.length;
//        let offset4 = offset3 + tag2.length;
//        let offset5 = offset4 + tag1.length + text3.length
//        expect(htmlIndices).toEqual([0,offset1, offset2, offset3, offset4, offset5]); 
//
//    });
//
//    it('extract text offsets', () => {
//        let [htmlIndices, htmlTags] = fileDisplay.splitHtml(innerHTML);
//
//        let r1 = fileDisplay.range(tag1.length,tag1.length+text1.length);
//        let next = r1[r1.length-1] + tag2.length + tag1.length + 1
//        let r2 = fileDisplay.range(next, next + text2.length);
//        next = r2[r2.length-1] + tag2.length + tag1.length+1;
//        let r3 = fileDisplay.range(next, next + text3.length);
//
//        let target = r1.concat(r2).concat(r3); 
//        let shifts = fileDisplay.getTagOffsets(innerText, htmlIndices, htmlTags);
//        expect(shifts.length).toEqual(innerText.length);
//        expect(shifts).toEqual(target);
// 
//    });
//
//    it('de- and-reconstruct a string', () => {
//        let [htmlIndices, htmlTags] = fileDisplay.splitHtml(innerHTML);
//        let shifts = fileDisplay.getTagOffsets(innerText, htmlIndices, htmlTags);
//        let reconstructed = fileDisplay.reconstruct(htmlTags, htmlIndices, shifts, innerText);
//        expect(reconstructed).toBe(innerHTML); 
//    });
//


    it('de- and-reconstruct a text layer where label is contained within html tags', () => {
        let highlighted = fileDisplay.highlightLayer(innerText, innerHTML, new RegExp(text2,"g"));
        expect(highlighted).toEqual(tag1 + text1 + tag2 + tag1 + openTag + text2 + closeTag + tag2 + tag1 + text3 + tag2);
    });

    it('de- and reconstruct a text layer where label is split across html tags', () => {
        let highlighted = fileDisplay.highlightLayer(innerText, innerHTML, new RegExp("notice text","g"));
        expect(highlighted).toEqual(tag1 + text1 + tag2 + tag1 + openTag + text2 + closeTag + tag2 + tag1 + openTag + text3 + closeTag + tag2);

    });


    it('testagain', () => {
        let d = " Limited was a Designated Foreign Issuer for the"
        let ih = "<div style=\"left: 68.04px; top: 64.6252px; font-size: 12.6px; font-family: sans-serif; transform: scaleX(1.0003);\">As of 1 July 2010, Mineral Deposits Limited is a Desi</div><div style=\"left: 361.44px; top: 64.6252px; font-size: 12.6px; font-family: sans-serif; transform: scaleX(1.00031);\">gnated Foreign Issuer as such term is defined in the </div><div style=\"left: 68.04px; top: 81.3958px; font-size: 12.6px; font-family: sans-serif; transform: scaleX(1.00051);\">Canadian Securities Administrato</div><div style=\"left: 254.37px; top: 81.3958px; font-size: 12.6px; font-family: sans-serif; transform: scaleX(1.00039);\">rs' National Instrument 71-102 </div><div style=\"left: 426.304px; top: 81.3958px; font-size: 12.6px; font-family: sans-serif; transform: scaleX(0.999747);\">Continuous Disclosure and Other </div><div style=\"left: 68.0274px; top: 98.242px; font-size: 12.6px; font-family: sans-serif; transform: scaleX(1.00007);\">Exemptions Relating to Foreign Issuers</div><div style=\"left: 287.986px; top: 98.242px; font-size: 12.6px; font-family: sans-serif; transform: scaleX(1.00106);\">.  Mineral Deposits Limited is s</div><div style=\"left: 458.912px; top: 98.242px; font-size: 12.6px; font-family: sans-serif; transform: scaleX(0.999426);\">ubject to the foreign regulatory </div><div style=\"left: 68.0274px; top: 115.013px; font-size: 12.6px; font-family: sans-serif; transform: scaleX(1.00016);\">requirements of the ASX and ASIC.  Mineral Deposits</div><div style=\"left: 367.044px; top: 115.013px; font-size: 12.6px; font-family: sans-serif; transform: scaleX(0.999975);\"> Limited was a Designated Foreign Issuer for the </div><div style=\"left: 68.0274px; top: 131.783px; font-size: 12.6px; font-family: sans-serif; transform: scaleX(1.00057);\">year ended 30 June 2010.   </div>";
let it = "As of 1 July 2010, Mineral Deposits Limited is a Designated Foreign Issuer as such term is defined in the Canadian Securities Administrators' National Instrument 71-102 Continuous Disclosure and Other Exemptions Relating to Foreign Issuers.  Mineral Deposits Limited is subject to the foreign regulatory requirements of the ASX and ASIC.  Mineral Deposits Limited was a Designated Foreign Issuer for the year ended 30 June 2010.   ";
        let highlighted = fileDisplay.highlightLayer(it, ih, new RegExp("Mineral Deposits Limited","g"));
//        console.log(highlighted);
      //  expect(highlighted).toEqual(a + "requirements of the ASX and ASIC.  " + openTag + "Mineral Deposits" + tag2 + c + " Limited" + closeTag + " was a Designated Foreign Issuer for the" + tag2);

    });

//    it('splice highlight spans into text and reconstruct', () => {
//        let labelIndices = [text1.length];
//        let highlighted = fileDisplay.highlightLayer(innerText, innerHTML, new RegExp("notice","g"));
//    });
//
//    it('splice highlight spans into text', () => {
//        let labelIndices = [text1.length];
//        let labels = [text2];
//        let shifts = fileDisplay.range(0, innerText.length);
//        
//        let [splicedText, splicedShifts] = fileDisplay.spliceHighlightText(innerText, labelIndices, labels, shifts, tag1, tag2);
//        expect(splicedText).toEqual(text1 + tag1 + text2 + tag2 + text3);
//        let shift1 = fileDisplay.range(0, text1.length);
//        let shift2 = fileDisplay.range(text1.length, text1.length + tag1.length + text2.length);
//        let shift3 = fileDisplay.range(shift2[shift2.length - 1] + 1, shift2[shift2.length-1]+tag2.length+text3.length + 1);
//        let concat = shift1.concat(shift2).concat(shift3);
//        expect(splicedShifts.length).toEqual(shifts.length + tag1.length + tag2.length);
//        expect(splicedShifts).toEqual(concat);
//    });
//
//    it('shifts are properly offset when splicing ', () => {
//        let labelIndices = [text1.length];
//        let innerText = text1 + text2 + text3;
//        let labels = [text2];
//        let shifts = fileDisplay.range(0, innerText.length);
//        let new_offset = 5;
//        for(let i = "Some noti".length; i < innerText.length; i++) {
//            shifts[i] += new_offset;
//        }
//        
//        let [splicedText, splicedShifts] = fileDisplay.spliceHighlightText(innerText, labelIndices, labels, shifts, openTag, closeTag);
//
//        expect(splicedText).toEqual(text1 + openTag + text2 + closeTag + text3);
//
//        let shift1 = fileDisplay.range(0, text1.length + openTag.length + "noti".length); // Some <span class="highlight">noti
//        let shift2 = fileDisplay.range(shift1[shift1.length - 1] + new_offset + 1, shift1[shift1.length -1] + new_offset + "ce".length + closeTag.length + text3.length + 1);
//        console.log(splicedShifts);
//        let concat = shift1.concat(shift2);
//        expect(splicedShifts.length).toEqual(shifts.length + openTag.length + closeTag.length);
//        expect(splicedShifts).toEqual(concat);
//    });
});



