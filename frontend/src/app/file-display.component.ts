import { OnInit, OnChanges, OnDestroy, Component, ViewChild, EventEmitter, Input, ElementRef } from '@angular/core';
import { AppComponent } from './app.component';
import { File, FileList } from './file';
import { Label, LabelList  } from './label';
import { Http }       from '@angular/http';
import { DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import { PDFJS } from "pdfjs-dist";

@Component({
	selector: 'file-display',
	templateUrl: './file-display.component.html',
	styleUrls: ['./file-display.component.css']
})

export class FileDisplayComponent implements OnChanges  {
	@Input() file: File;
	@Input() rightClickEmitter: EventEmitter<any>;
    pdfDoc: any;
    scale: number = 1.2;
    container: any;
	maxPages: number = 5;
	pdfError = false;

	constructor(private sanitizer: DomSanitizer, private http:Http, private elementRef:ElementRef) {

	}
	
	ngOnInit(): void {
	
	}

	ngOnChanges(): void {
		this.container = document.getElementById("pdf-wrapper");
		while (this.container.firstChild) {
			this.container.removeChild(this.container.firstChild);
		}
		
		this.pdfError = false;

        if(this.file) {
            let url = "/files/" + encodeURIComponent(this.file.id) + "/display";
            this.stream = this.sanitizer.bypassSecurityTrustResourceUrl(url);
            var thisComponent = this;
            PDFJS.getDocument(url).then(function(pdfDoc) {
				let pages = [];
                for(var num = 1; num <= pdfDoc.numPages; num++) { 
					let promise = pdfDoc.getPage(num).then((page) => {
						var pageContainer = document.createElement("div");
						pageContainer.className = "page-container";
						thisComponent.container.appendChild(pageContainer);
                        return thisComponent.renderPage(thisComponent.scale, page, pageContainer);
                    })
                    pages.push(promise);
                }
				return Promise.all(pages)
            }).then(function(pages) {
				thisComponent.highlightLabels();
			}).catch((error) => {
				thisComponent.pdfError = true;
			});
        }
    }
	
    range(start, finish) {
        let arr = Array.from(Array(finish - start).keys());
        for(let i = 0; i < arr.length; i++) {
            arr[i] += start;
        }
        return arr;
    }

    splitHtml(innerHTML) {
		let htmlRegex = new RegExp("</?[= a-zA-Z0-9;:\\-\\(\\)\\.\"]*>","g");
		let htmlIndices = [];
		let htmlTags = []
		let result;
		while(result = htmlRegex.exec(innerHTML)) {
			htmlIndices.push(result.index);
			htmlTags.push(result[0]);
		}
        return [htmlIndices, htmlTags]
    }

    splitLabels(innerText, regex) {
        let result;
		let labelIndices = [];
		let labels = [];
		let lastIdx;
		let last;
		while(result = regex.exec(innerText)) {
			labelIndices.push(result.index);
			labels.push(result[0]);
			
			// if(last) {
				// let overlap = last.length + lastIdx - result.index;
				// if(overlap > 0) {
					// labels[labels.length -2] = labels[labels.length -2].slice(0, labels[labels.length -2].length - overlap;
				// }
			// }
		}
        return [labelIndices, labels]
    }

    /*
    * Returns an array representing the offset of every character in innerText after inserting every htmlTag at every corresponding htmlIndices 
    */
    getTagOffsets(innerText, htmlIndices, htmlTags) {
		let shifts = Array.from(Array(innerText.length).keys());
        let accum = 0;	
        let currpos = 0;

		for(let i = 0; i < htmlTags.length - 1; i++) {
            let seq_length = htmlIndices[i+1] - htmlIndices[i] - htmlTags[i].length;
            accum += htmlTags[i].length;
			
            if(seq_length > 0) {
                for(let j=0;j<seq_length;j++) {
        		    shifts[currpos+j] += accum;
					
                }
                currpos += seq_length;
            }
		}
        return shifts;
    }

    reconstruct(htmlTags, htmlIndices, shifts, innerText, openTag, closeTag) {       
        var htmlArr = Array(shifts[shifts.length-1]).fill(0);
        for(let i=0; i < shifts.length; i++) {
            htmlArr[shifts[i]] = innerText[i];
        }

        let accum = 0;
        let iterator = htmlTags.join("")[Symbol.iterator]();
        for(let i = 0,j=0; i < shifts.length; i++,j++) {
            while(j < shifts[i]) {
                let next = iterator.next();
                if(next) {
                    htmlArr[j] = next.value;
                    j++;
                } else {
                    break;
                }
            }
        }
        htmlArr = htmlArr.concat(htmlTags[htmlTags.length-1])
        let htmlString = htmlArr.join("");
        
        let find = new RegExp("</?div[= a-zA-Z0-9;:\\-\\(\\)\\.\"]*>|"+openTag+"|"+closeTag,"g")
        let match;
        let last;
        let lastIdx;
        let open = 0;
        let copy = htmlString;
        while(match = find.exec(copy)) {
            let matchIdx = match.index + htmlString.length - copy.length;
            if(open > 0 && match[0].startsWith("</d")) {
                if(last == openTag) {
                    htmlString = htmlString.slice(0, matchIdx) + closeTag + htmlString.slice(matchIdx, htmlString.length);
                
                } else if(last.startsWith("<d")) {
                    htmlString = htmlString.slice(0, lastIdx+last.length) + openTag + htmlString.slice(lastIdx+last.length, matchIdx) + closeTag + htmlString.slice(matchIdx, htmlString.length);               
                }
            } else if(open > 0 && match[0] == closeTag && last != openTag) {
                htmlString = htmlString.slice(0, lastIdx + last.length) + openTag + htmlString.slice(lastIdx + last.length, htmlString.length);
            } 
            if(match[0] == openTag) {
                open++;
            } else if(match[0] == closeTag) {
                open--;
            }

            last = match[0];
            lastIdx = matchIdx;
        }

        return htmlString;
    }

    spliceHighlightText(innerText, labelIndices, labels, shifts, openTag, closeTag) {
		if(labelIndices.length == 0) {
			return [innerText, shifts];
		}
        let splicedText = innerText;
        let splicedShifts = shifts;
		let accum = 0;
						
		for(let i = 0; i < labelIndices.length; i++) {
			let original = splicedText;
			splicedText = original.slice(0, labelIndices[i] + accum);
			splicedText += openTag;
			splicedText += labels[i];
			splicedText += closeTag;
			splicedText += original.slice(labelIndices[i] + accum + labels[i].length, original.length);
          	accum += (openTag.length + closeTag.length);
		}	

        accum = 0;

        let first = splicedShifts.slice(0,labelIndices[0]);
        let subArrs = [first];

        for(let i=0; i < labels.length - 1; i++) {
            subArrs.push(splicedShifts.slice(labelIndices[i], labelIndices[i+1]));
        }
        let last = splicedShifts.slice(labelIndices[labelIndices.length - 1], splicedShifts.length);
        subArrs.push(last);
 
        for(let i=1; i < subArrs.length; i++) {
            if(i <= labels.length) {
                let startOpenTagIdx = subArrs[i][0] + accum;
                for(let j = openTag.length - 1; j >= 0; j--) {
                    subArrs[i].splice(0, 0, startOpenTagIdx + j);
                }
                let startCloseTagIdx = subArrs[i][labels[i-1].length + openTag.length -1] - closeTag.length + 1;
                for(let j = closeTag.length - 1; j >= 0; j--) {
                    subArrs[i].splice(labels[i-1].length + openTag.length,0, startCloseTagIdx + j);
                }
            }
            for(let j = openTag.length; j < subArrs[i].length; j++) {
                if( i <= labels.length) {
                    subArrs[i][j] += accum + openTag.length
                    if(j >= openTag.length + labels[i-1].length)  
                        subArrs[i][j] += closeTag.length;
                } else {
                    subArrs[i][j] += accum;
                }
            }
            accum += openTag.length + closeTag.length;
        }
        splicedShifts = [];
        for(let i =0; i < subArrs.length; i++) {
            splicedShifts = splicedShifts.concat(subArrs[i]);
        }
        return [splicedText, splicedShifts];
    }

    highlightLayer(innerText, innerHTML, regex) {
		let openTag = "<span class=\"highlight\">";
		let closeTag = "</span>";
		let openRegex = new RegExp(openTag,"g");
		let closeRegex = new RegExp(closeTag,"g");
		innerHTML = innerHTML.replace(openRegex,"").replace(closeRegex,"");	
		innerText = innerText.replace(openRegex,"").replace(closeRegex,"");	
		
		let [htmlIndices, htmlTags] = this.splitHtml(innerHTML);
        let [labelIndices, labels] = this.splitLabels(innerText, regex);
		
        let shifts = this.getTagOffsets(innerText, htmlIndices, htmlTags);
		
        let [splicedText, splicedShifts] = this.spliceHighlightText(innerText, labelIndices, labels, shifts, openTag, closeTag);
        return this.reconstruct(htmlTags, htmlIndices, splicedShifts, splicedText, openTag, closeTag);
    }
	
	highlightLabels() {
		this.file.getLabelsRegex().then((regex) => {
			if(regex) {
				for(let textLayer of this.container.getElementsByClassName("textLayer")) {			
					textLayer.innerHTML = this.highlightLayer(textLayer.innerText, textLayer.innerHTML, regex);
				}
			}
		});
	}

    renderPage(scale, page, pageContainer): void {
        var viewport = page.getViewport(scale);
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');
        var renderContext = {
          canvasContext: ctx,
          viewport: viewport
        };
        
        canvas.height = viewport.height;
        canvas.width = viewport.width;
        pageContainer.appendChild(canvas);
		
		var textDiv = document.createElement("div");
		textDiv.className = "textLayer";
		textDiv.style.height = viewport.height + "px";
		textDiv.style.width = viewport.width + "px";
		
		pageContainer.style.height = viewport.height + "px";
		pageContainer.style.width = viewport.width + "px";
		
		pageContainer.appendChild(textDiv);
		
        page.render(renderContext);
        return page.getTextContent().then(textContent => {
            return PDFJS.renderTextLayer({
                textContent:textContent,
                container:textDiv,
                viewport:viewport,
                textDivs: []
               });
        });
    }
	
	rightClick(e): void {
		console.log("Hijacking right click, so we've prevented context menu from displaying on right click.");
		e.preventDefault();
		let onSuccess = new EventEmitter<Label>();
		this.rightClickEmitter.emit({selectedText: window.getSelection().toString(), onSuccess:onSuccess});
		var thisComponent = this;
		onSuccess.subscribe((label) => {
			thisComponent.highlightLabels();	
		});
	}

   	stream: SafeResourceUrl;
}
