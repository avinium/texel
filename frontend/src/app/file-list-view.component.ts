import { OnInit, OnChanges, OnDestroy, Component, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { AppComponent } from './app.component';
import { File, FileList } from './file';
import { Label, LabelList  } from './label';

@Component({
	selector: 'file-list-view',
	templateUrl: './file-list-view.component.html',
	styleUrls: ['./app.component.css']
})

export class FileListViewComponent {
	@Input() selectedFiles: File[];
    @Input() fileList: FileList;
	@Input() confirmEmitter: EventEmitter<Object>;
	@Input() errorEmitter: EventEmitter<Object>;
    @Output() labelReset = new EventEmitter<File[]>();
	@Output() Select = new EventEmitter<File>();
    @Input() someProp: number;
	constructor() {}
    
    ngOnInit() {
        
    }
	
	resetLabels(): void {
		var onConfirm = new EventEmitter<boolean>();
		onConfirm.subscribe((response) => {            
			if(response.response == true) {
				this.fileList.resetLabels().then((files:File[]) => {
                    this.labelReset.emit(files);
                }).catch((e) => { this.confirmEmitter.emit({requestString:e}) });
			}
		});
		this.confirmEmitter.emit({responseEmitter:onConfirm, requestString:"Are you OK to reset all labels for all files?"});		
	}

}
