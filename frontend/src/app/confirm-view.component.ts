import { OnInit, OnDestroy, Component, ViewChild, EventEmitter, Input } from '@angular/core';

@Component({
	selector: 'confirm-view',
	templateUrl: './confirm-view.component.html',
	styleUrls: ['./app.component.css']
})

export class ConfirmViewComponent implements OnInit {
	
	@Input() requestEmitter: EventEmitter<Object>;
	showDialog: boolean;
	requestString: string;
	responseEmitter: EventEmitter<Object>;
	requestInput: boolean;
	constructor() { }
	
	ngOnInit(): void {
		this.requestEmitter.subscribe((event: Object) => {
            this.showDialog = true;
			this.requestString = event['requestString'];
			this.responseEmitter = event['responseEmitter'];
            this.requestInput = event['requestInput'];
		});
	}
    
    ok(input: string) {
        if(this.responseEmitter) {
            this.responseEmitter.emit({response:true, responseInput:input});
        }
        this.showDialog = false;
        this.requestInput = false;
    }
    cancel(input: string) {
        if(this.responseEmitter) {
            this.responseEmitter.emit({response:false, responseInput:input});
        }
        this.showDialog = false;
        this.requestInput = false;
    }
	
}
