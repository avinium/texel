import { OnInit, OnDestroy, OnChanges, Component, ViewChild, EventEmitter, Input, Output } from '@angular/core';
import { AppComponent } from './app.component';
import { File, FileList } from './file';
import { Label, LabelList  } from './label';

@Component({
	selector: 'label-list-view',
	templateUrl: './label-list-view.component.html',
	styleUrls: ['./app.component.css'],
})

export class LabelListViewComponent {
	
	@Input() labelList: LabelList;
	@Input() activeLabels: Label[];
    @Input() documentClickEmitter: EventEmitter<Object>;
	@Input() confirmEmitter: EventEmitter<Object>;
	@Output() Select = new EventEmitter<Label>();
    @Output() delAllEmitter = new EventEmitter<Object>();
    @Input() viewAllEmitter: EventEmitter<Label>;
    
	constructor() { 
	
	}
	
	save(name: string): void {
		this.labelList.save(name).catch(function(e) { this.confirmEmitter.emit({requestString:e}) });
    }
		
	delAll() : void {
		var onConfirm = new EventEmitter<boolean>();
		onConfirm.subscribe((response) => {
			if(response.response == true) {
				this.labelList.delAll().then((response) => {
                   this.delAllEmitter.emit({})
                }).catch(function(e) { this.confirmEmitter.emit({requestString:e}) });
			}
		})
		this.confirmEmitter.emit({responseEmitter:onConfirm, requestString:"Are you OK to delete all labels?"});
	}
    
    inputKey(event, newLabel) {
      if(event.keyCode == 13) {
        this.labelList.save(newLabel.value); 
        newLabel.value = '';
      }
    }
}


	

	